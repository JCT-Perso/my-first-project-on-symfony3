<?php 
namespace ApiBundle\Listener;

class ApiListener {

    public function __construct()
    {
        
    }

    public function redirectRequest($version)
    {
        $contoller = '';
        switch ($version) {
            case 'v1':
                $controller = 'v1Controller';
                break;

            case 'v2':
                $controller = 'v2Controller';
                break;
            
            default:
                break;
        }
        
        return $contoller;
    }
}
