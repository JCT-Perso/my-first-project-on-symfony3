<?php
namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class TeamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('rank', IntegerType::class, array('required' => false))
            ->add('save', SubmitType::class);;
    }
    
    public function getDefaultOptions(array $options)
    {
        return array('data_class' => 'ApiBundle\Entity\Team');
    }
    
    public function getName(){
        return 'Team';
    }
}