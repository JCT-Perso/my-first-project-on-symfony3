<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        
        $player = new \ApiBundle\Entity\Player();
        $player->setName('Jérémy');
        $player->setFirstName('Cornut');
        $player->setScore(50);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($player);
        $em->flush();
        
        return $this->render('ApiBundle:Default:index.html.twig', 
                array(
                    'version' => $request->query->get('version'),
                    'id' => $player->getId(),
                    'player' => $player
                )
            );
    }
}
