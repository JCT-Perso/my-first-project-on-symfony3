<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class teamController extends Controller
{
    public function indexAction(Request $request)
    {
        
        $team = new \ApiBundle\Entity\Team();
        $team->setName('Warriors');
        $team->setRank(1);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($team);
        $em->flush();
        
        return $this->render('ApiBundle:Default:index.html.twig', 
                array(
                    'version' => $request->query->get('version'),
                    'id' => $team->getId(),
                    'name' => $team->getName(),
                    'rank' => $team->getRank(),
                )
            );
    }
    
    /**
     * Return all teams
     */
    public function getTeamsAction() {
        // findAll()
        $response->setStatusCode(Response::HTTP_OK);
    }
    
    /**
     * Create team
     * 
     * @param array $params Contient les params de la request POST
     */
    public function postTeamAction($params) {
        
        $team = new \ApiBundle\Entity\Team();
        $team->setName($params['name']);
        $team->setRank($params['rank']);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($team);
        $em->flush();
        
        $response = new Response();
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }
    
    /**
     * Modify team
     * 
     * @param type $id
     */
    public function putTeamAction($id, $params) {
        /*
         * $repository = $this->getDoctrine()->getManager()
         *      ->getRepository('OCPlatformBundle:Advert');
         */
        // repository -> findById($id)
        $team->setName($params['name']);
        $team->setRank($params['rank']);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($team);
        $em->flush();
        
        $response = new Response();
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }

    /**
     * Return team with this param id
     * 
     * @param type $id
     */
    public function getTeamAction($id) {
        /*
         * $repository = $this->getDoctrine()->getManager()
         *      ->getRepository('OCPlatformBundle:Advert');
         */
        // repository -> findById($id) 
        
        $response = new Response();
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }
    
    /**
     * Delete team with this param id
     * 
     * @param type $id
     */
    public function deleteTeamAction($id) {
        /*
         * $repository = $this->getDoctrine()->getManager()
         *      ->getRepository('OCPlatformBundle:Advert');
         */
//$em->remove($advert);
//$em->flush(); // Exécute un DELETE sur $advert
        
        $response = new Response();
        $response->setStatusCode(Response::HTTP_OK);
        return $response;
    }
}
