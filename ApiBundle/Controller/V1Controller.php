<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\JsonResponse;
use ApiBundle\Entity\Team;
use ApiBundle\Form\TeamType;

class V1Controller extends Controller {

    public function indexAction(Request $request) {
        // Link Team pour aller voir toutes les 
        // méthodes en relation avec l'entité Team
    }

    public function getTeamAction($id) {

        $team = $this->getDoctrine()->getManager()
                        ->getRepository('ApiBundle:Team')->find($id);
        if (is_null($team) === true) {
            throw $this->createNotFoundException('Team not found with id : ' . $id);
        }

//        return $this->render('ApiBundle:Api:viewTeam.html.twig', array('team' => $team));
        return new JsonResponse($team, 200);
    }

    /**
     * Return all teams
     */
    public function getTeamsAction() {
        $teams = $this->getDoctrine()->getManager()
                        ->getRepository('ApiBundle:Team')->findAll();

        // Template prévu avant de revoir qu'il faut du json en réponse
        // return $this->render('ApiBundle:Api:viewTeams.html.twig', array('teams' => $teams));
        return new JsonResponse($teams, 200);
    }

    /**
     * Create team
     * 
     * @param Request $request
     */
    public function postTeamAction(Request $request) {
        $team = new Team();
        $formReq = $this->createForm(TeamType::class, $team);

        if ($formReq->handleRequest($request)->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($team);

                $em->flush();
            } catch (\Exception $ex) {
                return new JsonResponse(array('code' => $ex->getCode(), 'message' => $ex->getMessage()), 500);
            }

            return new JsonResponse(array('message' => 'Team have been saved.'));
        }

        // Sinon on affiche le form
        return $this->render('ApiBundle:Api:addTeam.html.twig', array(
                    'form' => $formReq->createView()
        ));
    }

    /**
     * Modify team
     * 
     * @param Request $request
     */
    public function putTeamAction(Request $request) {
        
        $team = new Team();
        $id = $request->query->get('id');
        if($id != false) {
            $team = $this->getDoctrine()->getManager()
                            ->getRepository('ApiBundle:Team')->find($id);
            $formReq = $this->createForm(TeamType::class, $team);
            if ($formReq->handleRequest($request)->isValid()) {
                try {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($team);

                    $em->flush();
                } catch (\Exception $ex) {
                    return new JsonResponse(array('code' => $ex->getCode(),
                        'message' => $ex->getMessage()), 400);
                }

                return new JsonResponse(array('message' => 'Team have been modified.'));
            }
        }else{
            return new JsonResponse(array('code' => 400414,
                        'message' => 'Team id not exist or not found with id : ' . $id), 400);
        }
        
        return $this->render('ApiBundle:Api:addTeam.html.twig', array('form' => $formReq->createView()));
    }

    /**
     * Delete team with this param id
     * 
     * @param type $id
     */
    public function deleteTeamAction($id) {
        try {
            $team = $this->getDoctrine()->getManager()
                            ->getRepository('ApiBundle:Team')->find($id);

            $em = $this->getDoctrine()->getManager();
            $em->remove($team);
            $em->flush();
        } catch (\Exception $ex) {
            return new JsonResponse(array('code' => $ex->getCode(),
                'message' => $ex->getMessage()), 400);
        }

        return new JsonResponse(array('message' => utf8_decode('Team have been deleted')), 200);
    }

}
