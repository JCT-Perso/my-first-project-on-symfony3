<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use \Symfony\Component\HttpFoundation\JsonResponse;

class V2Controller extends V1Controller
{
    public function indexAction(Request $request)
    {
        
    }

    public function putTeamAction(Request $request) {
        return new JsonResponse(array('code' => 400424,
                        'message' => 'This method don\'t exist in this Api version', 'request' => $request), 400);
    }
}
