<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DispatcherController extends Controller
{
    public function findVersion($version)
    {
            switch ($version) {
                case 'v1':
                case 'V1':
                    $controller = 'V1';
                    break;

                case 'v2':
                case 'V2':
                    $controller = 'V2';
                    break;

                default:
                    $controller = '';
                    break;
            }
            
        return $controller;
    }
    
    public function redirectTeamAction($version, $id, $ressource, Request $request) 
    {
        $controller = $this->findVersion($version);
        $request->query->set('id', $id);
        if ($controller == ''){
            //throw $this->createNotFoundException('Api version not exist.');
            return new JsonResponse(array('code' => 514, 'message' => 'Api version not exist.'), 500);
        }
        
        return $this->forward('ApiBundle:'.$controller.':'.$ressource.'Team', array('request' => $request));
    }
    
    public function redirectTeamsAction($version, $id, $ressource) 
    {
        $controller = $this->findVersion($version);
        
        $params['id'] = $id;
        if ($controller == ''){
            //throw $this->createNotFoundException('Api version not exist.');
            return new JsonResponse(array('code' => 514, 'message' => 'Api version not exist.'), 500);
        }
        
        return $this->forward('ApiBundle:'.$controller.':'.$ressource.'Teams', $params);
    }
}
