<?php

namespace ApiBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ApiBundle\Entity\Team;

class LoadTeam implements FixtureInterface {

    public function load(ObjectManager $manager) {
        // Liste des noms des équipes à ajouter
        $names = array(
            1 => 'Warriors',
            2 => 'Cavaliers',
            3 => 'Spurs',
            4 => 'Heats',
            5 => 'Raptors',
            6 => 'Trail Blazers',
            7 => 'Haws',
            8 => 'Thunder'
        );

        foreach ($names as $rank => $name) {
            // On crée la compétence
            $team = new Team();
            $team->setName($name);
            $team->setRank($rank);

            // On la persiste
            $manager->persist($team);
        }

        // On déclenche l'enregistrement de toutes les équipes
        $manager->flush();
    }

}
