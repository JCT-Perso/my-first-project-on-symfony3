<?php

namespace JCTTechnicExoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class ApiController extends Controller
{
    public function indexAction()
    {
        $params = array(
            'titre_page' => 'My first page',
            'navigation' => array(
            1 => array('prénom' => 'Jérémy', 'nom' => 'Cornut'), 
            2 => array('prénom' => 'Toto', 'nom' => 'Coco')));
        
        $content = $this->get('templating')->render('JCTTechnicExoBundle:Api:index.html.twig', $params);
        
        return new Response($content);
    }
    
    
    public function viewAction($id)
    {
        return $this->render('JCTTechnicExoBundle:Api:view.html.twig', array(
          'id' => $id
        ));
    }
    
  // Ajoutez cette méthode :
  public function addAction(Request $request)
  {
    $session = $request->getSession();
    
    // test juste des messages
    
    // Mais faisons comme si c'était le cas
    $session->getFlashBag()->add('info', 'Annonce bien enregistrée');

    // Le « flashBag » est ce qui contient les messages flash dans la session
    // Il peut bien sûr contenir plusieurs messages :
    $session->getFlashBag()->add('info', 'Oui oui, elle est bien enregistrée !');

    // Puis on redirige vers la page de visualisation de cette annonce
    return $this->redirectToRoute('hello_api', array('id' => 14));
  }
    
    public function testSessionAction($id, Request $request)
    {
        // Récupération de la session
        $session = $request->getSession();

        // On récupère le contenu de la variable user_id
        $userId = $session->get('user_id');

        // On définit une nouvelle valeur pour cette variable user_id
        $session->set('user_id', $id);

        // On n'oublie pas de renvoyer une réponse
        return new Response("Je suis une page de test, je n'ai rien à dire sauf idUser = " .$userId);
    }
    
    public function testJsonAction($id)
    {
      return new JsonResponse(array('id' => $id));
    }
}
