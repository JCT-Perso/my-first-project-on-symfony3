<?php

namespace JCTTechnicExoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('JCTTechnicExoBundle:Default:index.html.twig',
                array('name' => $name));
    }
    
    public function byebyeAction($name,Request $resquest)
    {
        $type = $resquest->query->get('type');
        if ($type === '') {
            $type = false;
        }
        return $this->render('JCTTechnicExoBundle:Default:byebye.html.twig',
                array('name' => $name, 'type' => $type));
    }
}
